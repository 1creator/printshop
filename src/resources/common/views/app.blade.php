<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Клиент</title>
    <link href="{{ mix('static/css/vendor.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ mix('static/css/common.css') }}" rel="stylesheet" type="text/css">
    <link href="https://file.myfontastic.com/P8Y4jwtesfg6PpcQGMnxm8/icons.css" rel="stylesheet">
</head>
<body>
<div id="app"></div>
<script>
    window.initialState = {};
    @isset($user)
        window.initialState.user = {!! json_encode($user) !!};
    window.initialState.goods = {!! json_encode($goods) !!};
    window.initialState.statuses = {!! json_encode($statuses) !!};
    window.initialState.agents = {!! json_encode($agents) !!};
    @endisset
</script>
<script src="{{ mix('static/js/user.js') }}"></script>
</body>
</html>
