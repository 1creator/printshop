/**
 * @typedef {Object} ProductGroup
 * @property {number} id
 * @property {slug} slug
 * @property {string} name
 * @property {string} description
 * @property {number} likes
 * @property {number} orderWeight
 * @property {array} products
 * @property {string} createdAt
 */

export default {
    /**
     * @returns {ProductGroup}
     */
    create(props = {}) {
        return {
            ...{
                id: Math.random().toString(36).substr(2, 5),
                slug: null,
                name: null,
                description: null,
                likes: 0,
                orderWeight: null,
                products: [],
                createdAt: null,
                isHot: false,
            },
            ...JSON.parse(JSON.stringify(props)),
        };
    }

}
