/**
 * @typedef {Object} Promo
 * @property {number} id
 * @property {string} name
 * @property {string} description
 * @property {string} createdAt
 */

export default {
    /**
     * @returns {Promo}
     */
    create(props = {}) {
        return {
            ...{
                id: null,
                name: null,
                description: null,
                createdAt: null,
            },
            ...JSON.parse(JSON.stringify(props)),
        };
    }

}
