/**
 * @typedef {Object} Good
 * @property {number} id
 * @property {string} name
 * @property {string} priceRub
 * @property {string} description
 * @property {boolean} active
 */

export default {
    /**
     * @returns {Good}
     */
    create(props = {}) {
        return {
            ...{
                // id: Math.random().toString(36).substr(2, 5),
                name: null,
                priceRub: null,
                description: "",
                active: true,
            },
            ...JSON.parse(JSON.stringify(props)),
        };
    }

}
