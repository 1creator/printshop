import Vue from "vue";
import store from "@/common/js/store/store";
import "@/common/js/directives/vMask";
import App from "@/common/js/components/App";
import router from "@/common/js/router";
import {Multiselect} from "vue-multiselect";
import moment from "moment";

Vue.component("vue-multiselect", Multiselect);

window.app = new Vue({
    el: "#app",
    store: store,
    router: router,
    components: {},
    render: h => h(App)
});

moment.locale("ru");
