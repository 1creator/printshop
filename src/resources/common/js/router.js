import Vue from "vue";
import VueRouter from "vue-router";
import TheAuth from "@/common/js/components/auth/TheAuth";
import TheOrders from "@/common/js/components/orders/TheOrders";
import TheProfile from "@/common/js/components/profile/TheProfile";
import store from "@/common/js/store/store";
import TheAgents from "@/common/js/components/agents/TheAgents";

Vue.use(VueRouter);

export const routes = [
    {
        path: "/",
        redirect: "/orders"
    },

    {
        path: "/orders",
        component: TheOrders,
        name: "orders"
    },

    {
        path: "/agents",
        component: TheAgents,
        name: "agents"
    },

    {
        path: "/profile",
        component: TheProfile,
        name: "profile"
    },

    {
        path: "/login",
        component: TheAuth,
        name: "login"
    },
];

const router = new VueRouter({
    mode: "history",
    routes: routes,
    base: "client",
});

router.beforeEach((to, from, next) => {
    if (to.name != "login" && !store.getters["auth/authorized"]) {
        router.push({name: "login"});
    }
    store.commit("sidebar/toggle", false);
    next()
});

export default router;
