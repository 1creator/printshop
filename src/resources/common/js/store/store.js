import Vue from "vue";
import Vuex from "vuex";
import auth from "@/common/js/store/auth";
import sidebar from "@/common/js/store/sidebar";
import statuses from "@/common/js/store/statuses";
import goods from "@/common/js/store/goods";
import agents from "@/common/js/store/agents";

Vue.use(Vuex);

const store = new Vuex.Store({
    strict: process.env.NODE_ENV !== "production",
    state: {
        ready: true,
    },
    modules: {
        auth, sidebar, statuses, goods, agents,
    },
    mutations: {
        setReady(state, val) {
            state.ready = val;
        }
    },
    actions: {
        /*    async fetchDashboard(context) {
                context.commit('setReady', false);
                try {

                } catch (e) {

                }
                context.commit('setReady', true);
            },*/
    },
});

export default store;
