const state = {
    items: window.initialState.goods || [],
};

const getters = {
    findById: state => id => state.items.find(item => item.id == id),
};

const actions = {

};

const mutations = {

};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
