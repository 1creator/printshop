const state = {
    items: window.initialState.statuses || []
};

const getters = {
    findById: state => id => state.items.find(item => item.id == id),
    findByIdSafe: state => id => {
        return state.items.find(item => item.id == id) || {
            id: null,
            name: "Статус не определен",
            color: "#545454",
            background: "#ffdfdf",
        };
    },
    order: state => state.items.filter(item => item.type === "order"),
    orderItem: state => state.items.filter(item => item.type === "order_item"),
};

const actions = {};

const mutations = {};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
