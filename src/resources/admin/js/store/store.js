import Vue from "vue";
import Vuex from "vuex";
import auth from "@/admin/js/store/auth";
import sidebar from "@/common/js/store/sidebar";
import employers from "@/admin/js/store/employers";
import goods from "@/admin/js/store/goods";
import statuses from "@/admin/js/store/statuses";

Vue.use(Vuex);

const store = new Vuex.Store({
    strict: process.env.NODE_ENV !== "production",
    state: {
        ready: true,
    },
    modules: {
        auth, sidebar, employers, goods, statuses,
    },
    mutations: {
        setReady(state, val) {
            state.ready = val;
        }
    },
    actions: {
        /* async fetchDashboard(context) {
             context.commit("setReady", false);
             const res = await dashboard.fetch();
             context.commit("goods/setItems", res.goods);
             context.commit("setReady", true);
         },*/
    },
});

export default store;
