/**
 * @typedef {Object} Agent
 * @property {number} id
 * @property {string} name
 * @property {boolean} active
 * @property {number} clientId
 * @property {Object} client
 * @property {number} managerId
 * @property {number} workerId
 * @property {string} tgCode
 * @property {number} discount
 * @property {string} createdAt
 */

export default {
    /**
     * @returns {Agent}
     */
    create(props = {}) {
        return {
            ...{
                id: null,
                name: null,
                active: true,
                clientId: null,
                client: null,
                managerId: null,
                workerId: null,
                tgCode: null,
                discount: 0,
                createdAt: null,
            },
            ...JSON.parse(JSON.stringify(props)),
        };
    }

}
