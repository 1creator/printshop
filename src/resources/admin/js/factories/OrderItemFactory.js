/**
 * @typedef {Object} OrderItem
 * @property {number} id
 * @property {number} orderId
 * @property {number} goodId
 * @property {number} statusId
 * @property {number} count
 * @property {number} priceRub
 */

export default {
    /**
     * @returns {OrderItem}
     */
    create(props = {}) {
        return {
            ...{
                id: null,
                orderId: null,
                order: null,
                goodId: null,
                good: null,
                statusId: null,
                status: null,
                count: 1,
                priceRub: null,
                createdAt: null,
            },
            ...JSON.parse(JSON.stringify(props)),
        };
    }

}
