/**
 * @typedef {Object} Order
 * @property {number} id
 * @property {number} managerId
 * @property {number} agentId
 * @property {number} userId
 * @property {number} statusId
 * @property {number} discount
 * @property {object[]} items
 * @property {string} comment
 * @property {string} deliveryAddress
 */

export default {
    /**
     * @returns {Order}
     */
    create(props = {}) {
        return {
            ...{
                id: null,
                managerId: null,
                agentId: null,
                statusId: null,
                comment: null,
                discount: 0,
                deliveryAddress: null,
                items: [],
                createdAt: null,
            },
            ...JSON.parse(JSON.stringify(props)),
        };
    }

}
