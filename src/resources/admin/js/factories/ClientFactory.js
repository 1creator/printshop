/**
 * @typedef {Object} Client
 * @property {number} id
 * @property {string} name
 * @property {string} login
 * @property {number} password
 * @property {string} createdAt
 */

export default {
    /**
     * @returns {Client}
     */
    create(props = {}) {
        return {
            ...{
                id: null,
                name: null,
                login: null,
                password: undefined,
                passwordConfirmation: undefined,
                createdAt: null,
            },
            ...JSON.parse(JSON.stringify(props)),
        };
    }

}
