<?php


namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Http\Middleware\TransformRequestPhone;
use App\Services\Employer\Employer;
use App\Services\Employer\EmployerService;
use Illuminate\Http\Request;

class EmployerController extends Controller
{
    public function __construct()
    {
        $this->middleware(TransformRequestPhone::class);
    }

    public function index(Request $request)
    {
        return Employer::all();
    }

    public function store(Request $request)
    {
        return app(EmployerService::class)->store($request->all());
    }

    public function update(Request $request, Employer $employer)
    {
        return app(EmployerService::class)->update($employer, $request->all());
    }

    public function destroy(Request $request, Employer $employer)
    {
        return $employer->delete() ? 1 : 0;
    }
}
