<?php


namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Services\Agent\Agent;
use App\Services\Agent\AgentService;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;

class AgentController extends Controller
{
    public function index(Request $request)
    {
        return QueryBuilder::for(Agent::class)
            ->allowedIncludes('client')
            ->allowedFilters(['client_id'])
            ->get();
    }

    public function show(Request $request, $agent)
    {
        return QueryBuilder::for(Agent::class)
            ->allowedIncludes('client')
            ->findOrFail($agent);
    }

    public function store(Request $request)
    {
        return app(AgentService::class)->store($request->all());
    }

    public function update(Request $request, Agent $agent)
    {
        return app(AgentService::class)->update($agent, $request->all());
    }

    public function destroy(Request $request, Agent $agent)
    {
        return $agent->delete() ? 1 : 0;
    }
}
