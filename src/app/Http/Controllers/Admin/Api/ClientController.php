<?php


namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Http\Middleware\TransformRequestPhone;
use App\Services\Client\Client;
use App\Services\Client\ClientService;
use App\Utils\QueryBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware(TransformRequestPhone::class);
    }

    public function index(Request $request)
    {
        return QueryBuilder::for(Client::class)
            ->allowedFilters(AllowedFilter::callback('query', function (Builder $builder, $val) {
                $builder->where('name', 'LIKE', '%' . $val . '%');
            }))
            ->allowedIncludes(['agents'])
            ->get();
    }

    public function store(Request $request)
    {
        return app(ClientService::class)->store($request->all());
    }

    public function update(Request $request, Client $client)
    {
        return app(ClientService::class)->update($client, $request->all());
    }

    public function show(Request $request, $client)
    {
        return QueryBuilder::for(Client::class)
            ->allowedIncludes(['agents'])
            ->findOrFail($client);
    }

    public function destroy(Request $request, Client $client)
    {
        return $client->delete() ? 1 : 0;
    }
}
