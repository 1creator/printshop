<?php

namespace App\Http\Controllers\Admin\Api;

use App\Services\Client\Good;

class DashboardController
{
    public function index()
    {
        $goods = Good::all();
        return [
            'goods' => $goods,
        ];
    }
}
