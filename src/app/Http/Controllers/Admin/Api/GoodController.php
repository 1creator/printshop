<?php


namespace App\Http\Controllers\Admin\Api;


use App\Http\Controllers\Controller;
use App\Services\Shop\GoodService;
use App\Services\Shop\Good;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;

class GoodController extends Controller
{
    public function index(Request $request)
    {
        return QueryBuilder::for(Good::class)->get();
    }

    public function store(Request $request)
    {
        return app(GoodService::class)->store($request->all());
    }

    public function show(Request $request, Good $good)
    {
        return $good;
    }

    public function update(Request $request, Good $good)
    {
        return app(GoodService::class)->update($good, $request->all());
    }

    public function destroy(Request $request, Good $good)
    {
        return app(GoodService::class)->destroy($good);
    }
}
