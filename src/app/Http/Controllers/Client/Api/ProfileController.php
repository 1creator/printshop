<?php


namespace App\Http\Controllers\Client\Api;


use App\Http\Controllers\Controller;
use App\Http\Middleware\TransformRequestPhone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(TransformRequestPhone::class);
    }

    public function index(Request $request)
    {
        return Auth::guard('web')->user();
    }

    public function update(Request $request)
    {
        $client = Auth::guard('web')->user();
        $request->validate([
            'name' => 'sometimes|required|string|max:100',
            'login' => 'sometimes|string|max:100|unique:clients,login,' . $client->id,
            'password' => 'sometimes|string|min:4|confirmed',
        ]);

        $client->fill($request->only([
            'name', 'login', 'password',
        ]));
        if ($request->filled('password')) {
            $client->password = Hash::make($request->input('password'));
        }
        $client->save();
        return $client;
    }
}
