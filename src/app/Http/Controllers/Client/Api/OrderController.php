<?php


namespace App\Http\Controllers\Client\Api;

use App\Http\Controllers\Controller;
use App\Services\Shop\Order;
use App\Services\Shop\OrderService;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $agentIds = Auth::user()->agents->pluck('id');

        return QueryBuilder::for(Order::class)
            ->allowedIncludes(['items', 'agent'])
            ->whereIn('agent_id', $agentIds)
            ->defaultSort('-created_at')
            ->get();
    }

    public function store(Request $request)
    {
        return app(OrderService::class)->store($request->all());
    }

    public function update(Request $request, Order $order)
    {
        if ($order->user_id != Auth::id())
            abort(403);
        return app(OrderService::class)->update($order, $request->all());
    }

    public function destroy(Request $request, Order $order)
    {
        if ($order->user_id != Auth::id())
            abort(403);
        return $order->delete() ? 1 : 0;
    }
}
