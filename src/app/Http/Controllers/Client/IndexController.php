<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Services\Agent\Agent;
use App\Services\Employer\Employer;
use App\Services\Shop\Good;
use App\Services\Shop\Status;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index()
    {
        $data = [];

        $user = Auth::guard('web')->user();
        if ($user) {
            $statuses = Status::all();
            $goods = Good::where('active', 1)->get();
            $agents = Agent::where('client_id', $user->id)->get();
            $data = collect([
                'user' => $user,
                'statuses' => $statuses,
                'goods' => $goods,
                'agents' => $agents,
            ]);
            $data = $data->camelCaseKeys();
        }

        return view('common.views.app', $data);
    }

    public function logout()
    {
        Auth::guard('web')->logout();
        return redirect('/client/login');
    }
}
