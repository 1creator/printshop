<?php


namespace App\Services\Employer;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Employer extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $hidden = ['password', 'remember_token'];
    protected $guarded = ['id'];
}
