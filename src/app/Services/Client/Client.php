<?php


namespace App\Services\Client;


use App\Services\Agent\Agent;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;

class Client extends Authenticatable
{
    use HasApiTokens;

    protected $hidden = ['password', 'remember_token'];
    protected $guarded = ['id'];

    public function agents()
    {
        return $this->hasMany(Agent::class);
    }

//    public function lastOrder()
//    {
//        return $this->hasOne(Order::class)->latest();
//    }
}
