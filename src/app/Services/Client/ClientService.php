<?php


namespace App\Services\Client;


use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ClientService
{
    public function store(array $data)
    {
        Validator::validate($data, [
            'name' => 'string|max:100',
            'login' => 'string|alpha_dash|max:16|unique:clients,login',
            'password' => 'required_if:has_access,true|nullable|string|min:4|confirmed',
        ]);

        $data = collect($data);
        $client = new Client($data->only(['name', 'login'])->toArray());
        $client->password = Hash::make($data['password']);
        $client->save();

        return $client;
    }

    public function update(Client $client, array $data)
    {
        Validator::validate($data, [
            'name' => 'sometimes|required|string|max:100',
            'login' => 'sometimes|string|alpha_dash|max:16|unique:clients,login,' . $client->id,
            'password' => 'sometimes|nullable|confirmed|min:4|string',
        ]);

        $data = collect($data);
        $client->fill($data->only(['name', 'login'])->toArray());
        if ($data['password'] ?? null) {
            $client->password = Hash::make($data['password']);
        }
        $client->save();

        return $client;
    }
}
