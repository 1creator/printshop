<?php


namespace App\Services\Attachment\Components;

use App\Services\Attachment\Models\Attachment;
use Illuminate\View\Component;

class AttachmentComponent extends Component
{
    public Attachment $attachment;

    /**
     * Create the component instance.
     *
     * @param Attachment $attachment
     */
    public function __construct(Attachment $attachment)
    {
        $this->attachment = $attachment;
    }

    public function render()
    {
        return view('attachment::attachment-view');
    }
}
