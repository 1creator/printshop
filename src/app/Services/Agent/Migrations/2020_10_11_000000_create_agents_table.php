<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('tg_code');
            $table->decimal('discount', 5, 2)->default(0);
            $table->foreignId('client_id')->references('id')
                ->on('clients')->cascadeOnDelete()->cascadeOnUpdate();
            $table->unsignedBigInteger('worker_id')->nullable();
            $table->unsignedBigInteger('manager_id')->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}
