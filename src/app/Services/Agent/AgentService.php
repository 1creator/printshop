<?php


namespace App\Services\Agent;


use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AgentService
{
    public function store(array $data)
    {
        Validator::validate($data, [
            'name' => 'required|string|max:100',
            'active' => 'boolean',
            'tg_code' => 'required|string|max:255',
            'client_id' => 'required|exists:clients,id',
            'worker_id' => 'nullable|integer',
            'manager_id' => 'nullable|integer',
        ]);

        $data = collect($data);
        $agent = new Agent($data->only(['name', 'active', 'tg_code', 'client_id', 'worker_id', 'manager_id', 'discount'])->toArray());
        $agent->save();

        return $agent;
    }

    public function update(Agent $agent, array $data)
    {
        Validator::validate($data, [
            'name' => 'required|string|max:100',
            'active' => 'boolean',
            'tg_code' => 'required|string|max:255',
            'client_id' => 'required|exists:clients,id',
            'worker_id' => 'nullable|integer',
            'manager_id' => 'nullable|integer',
        ]);

        $data = collect($data);
        $agent->fill($data->only(['name', 'active', 'tg_code', 'client_id', 'worker_id', 'manager_id', 'discount'])->toArray());
        $agent->save();

        return $agent;
    }
}
