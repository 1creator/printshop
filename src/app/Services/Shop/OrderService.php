<?php


namespace App\Services\Shop;


use Exception;
use Illuminate\Support\Facades\Validator;

class OrderService
{
    public function store(array $data): Order
    {
        $data = collect($data);
        Validator::validate($data->toArray(), [
            'agent_id' => 'required|exists:agents,id',
            'status_id' => 'nullable|exists:statuses,id',
            'manager_id' => 'nullable|integer',
            'delivery_address' => 'nullable|string|max:255',
            'comment' => 'nullable|string|max:255',
            'items' => 'array|required|min:1',
            'discount' => 'nullable|numeric|min:0|max:100',
        ]);

        $order = new Order($data->only([
            'name', 'agent_id', 'manager_id', 'status_id', 'delivery_address', 'comment', 'discount'
        ])->toArray());
        $order->save();

        foreach ($data['items'] as $item) {
            $good = Good::findOrFail($item['good_id']);
            $orderItem = new OrderItem(collect($item)->only([
                'good_id', 'count', 'price_rub', 'status_id'
            ])->toArray());
            if (!$orderItem->price_rub) {
                $orderItem->price_rub = $good->price_rub;
            }
            $order->items()->save($orderItem);
        }

        $order->load(['items', 'agent.client']);

        return $order;
    }

    public function update(Order $order, array $data): Order
    {
        $data = collect($data);
        Validator::validate($data->toArray(), [
            'agent_id' => 'sometimes|required|exists:agents,id',
            'status_id' => 'sometimes|nullable|exists:statuses,id',
            'manager_id' => 'sometimes|nullable|integer',
            'delivery_address' => 'sometimes|nullable|string|max:255',
            'comment' => 'sometimes|nullable|string|max:255',
            'items' => 'sometimes|array|required|min:1',
            'discount' => 'sometimes|nullable|numeric|min:0|max:100',
        ]);

        $order->update($data->only([
            'name', 'agent_id', 'manager_id', 'status_id', 'delivery_address', 'comment', 'discount'
        ])->toArray());

        if ($data->has('items')) {
            $itemIds = [];
            foreach ($data['items'] as $item) {
                $itemModel = null;
                $id = $item['id'] ?? null;
                if ($id) {
                    $itemModel = $order->items->where('id', $id)->first();
                }
                if ($itemModel) {
                    $itemModel['good_id'] = $item['good_id'];
                    $itemModel['count'] = $item['count'];
                    $itemModel['price_rub'] = $item['price_rub'];
                    $itemModel['status_id'] = $item['status_id'];
                    $itemModel->save();
                } else {
                    $good = Good::findOrFail($item['good_id']);
                    $itemModel = new OrderItem([
                        'order_id' => $order['id'],
                        'good_id' => $item['good_id'],
                        'count' => $item['count'],
                        'price_rub' => $item['price_rub'] ?? $good->price_rub,
                        'status_id' => $item['status_id'],
                    ]);
                    $itemModel->save();
                }
                array_push($itemIds, $itemModel['id']);
            }
            $order->items()->whereNotIn('id', $itemIds)->delete();
        }

        $order->load(['items', 'agent.client']);

        return $order;
    }

    public function destroy(Order $order): bool
    {
        try {
            return $order->delete() ? 1 : 0;
        } catch (Exception $e) {
            return 0;
        }
    }
}
