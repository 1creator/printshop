<?php


namespace App\Services\Shop;


use App\Services\Agent\Agent;
use App\Services\Employer\Employer;
use App\Services\Client\Client;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = ['id'];
    protected $appends = ['total', 'amount'];
//    protected $with = ['status'];

//    public function orders()
//    {
//        return $this->hasMany(Order::class);
//    }
//
//    public function lastOrder()
//    {
//        return $this->hasOne(Order::class)->latest();
//    }


    public function getAmountAttribute()
    {
        return $this->items->reduce(function ($acc, $item) {
            $acc += $item['price_rub'] * $item['count'];
            return $acc;
        }, 0);
    }

    public function getTotalAttribute()
    {
        $amount = $this->amount;
        return $this->discount > 0 ?
            $amount - $this->discount * $amount / 100 :
            $amount;
    }

    public function status()
    {
        return $this->belongsTo(Status::class)->withDefault([
            'name' => 'Статус не определен',
        ]);
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class)->withDefault([
            'name' => 'Агент не указан',
        ]);
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }
}
