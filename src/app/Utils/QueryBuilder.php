<?php


namespace App\Utils;


class QueryBuilder extends \Spatie\QueryBuilder\QueryBuilder
{

    /**
     * {@inheritdoc}
     */
    public function get($columns = ['*'])
    {
        if ($this->request->input('limit')) {
            $this->limit($this->request->input('limit'));
        }
        if ($this->request->input('offset')) {
            $this->offset($this->request->input('offset'));
        }

        $results = parent::get($columns);

        if ($this->request->appends()->isNotEmpty()) {
            $results = $this->addAppendsToResults($results);
        }

        return $results;
    }
}
