<?php

use App\Http\Controllers\Admin\Api\AgentController;
use App\Http\Controllers\Admin\Api\AuthController;
use App\Http\Controllers\Admin\Api\DashboardController;
use App\Http\Controllers\Admin\Api\EmployerController;
use App\Http\Controllers\Admin\Api\GoodController;
use App\Http\Controllers\Admin\Api\OrderController;
use App\Http\Controllers\Admin\Api\ProfileController;
use App\Http\Controllers\Admin\Api\StatusController;
use App\Http\Controllers\Admin\Api\ClientController;
use App\Http\Controllers\Client\Api\AttachmentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'admin',
], function () {
    Route::post('login', [AuthController::class, 'login']);

    Route::group([
        'middleware' => ['auth:admin'],
    ], function () {
        Route::get('dashboard', [DashboardController::class, 'index']);
        Route::put('profile', [ProfileController::class, 'update']);

        Route::resources([
            'goods' => GoodController::class,
            'clients' => ClientController::class,
            'employers' => EmployerController::class,
            'agents' => AgentController::class,
            'statuses' => StatusController::class,
            'orders' => OrderController::class,
        ]);
    });
});

Route::post('upload', [AttachmentController::class, 'store']);

Route::group([
    'prefix' => 'client',
], function () {
    Route::post('login', [\App\Http\Controllers\Client\Api\AuthController::class, 'login']);

    Route::group([
        'middleware' => ['auth:web'],
    ], function () {
        Route::put('profile', [\App\Http\Controllers\Client\Api\ProfileController::class, 'update']);

        Route::resources([
            'orders' => \App\Http\Controllers\Client\Api\OrderController::class,
        ]);
    });
});
