<?php

use App\Http\Controllers\Admin\IndexController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin/logout', [IndexController::class, 'logout']);
Route::get('/admin{any}', [IndexController::class, 'index'])->where('any', '.*');

Route::get('/client/logout', [\App\Http\Controllers\Client\IndexController::class, 'logout']);
Route::get('/client{any}', [\App\Http\Controllers\Client\IndexController::class, 'index'])
    ->where('any', '.*');
//Route::get('/user{any}', [IndexController::class, 'index'])->where('any', '.*');
