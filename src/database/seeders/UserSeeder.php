<?php

namespace Database\Seeders;

use App\Services\Employer\Employer;
use App\Services\Client\Client;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employers = [
            [
                'phone' => '79825578582',
                'email' => 'admin@1creator.ru',
                'first_name' => 'Суперадмин',
                'last_name' => 'Суперадмин',
                'password' => \Illuminate\Support\Facades\Hash::make('password'),
            ],
        ];

        $clients = [
            [
                'name' => 'Клиент',
                'login' => 'client',
                'password' => \Illuminate\Support\Facades\Hash::make('password'),
            ],
        ];

        foreach ($employers as $employer) {
            Employer::create($employer);
        }

        foreach ($clients as $client) {
            Client::create($client);
        }
    }
}
